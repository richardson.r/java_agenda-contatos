package br.com.itau;

public class DadosContato {
    private TipoDado tipo;
    private String valor;

    public String getTipo() {
        return tipo.name();
    }

    public void setTipo(TipoDado tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public DadosContato(TipoDado tipo, String valor){
        this.tipo = tipo;
        this.valor = valor;
    }
}
