package br.com.itau;

import java.io.IOException;
import java.util.Scanner;

public class IO {
    public static void inicio(){
        System.out.println("**********************************************************");
        System.out.println("* Bem vindo ao sistema de cadastro de agendas e contatos *");
        System.out.println("**********************************************************\n");

        menuInicial();
    }
    private static void menuInicial(){
        System.out.println("O que deseja fazer:");
        System.out.println(" (1) Criar uma agenda ");
        System.out.println(" (2) Exibir agenda ");
        System.out.println(" (3) Editar agenda ");
        System.out.println(" (4) Buscar contato ");
        System.out.println(" (0) Sair ");

        Integer selecao;

        do {
            selecao = IO.scanner().nextInt();
        }
        while (selecao < 0 || selecao > 4);

        if(selecao == 1)
            criarAgenda();
    }

    private static void criarAgenda(){
        System.out.println("Digite o nome da agenda: ");
        String nome = IO.scanner().nextLine();

        AgendaContatos agenda = new AgendaContatos(nome);
        agenda.adicionar();

        System.out.println("\nAgenda criada com sucesso.");
        IO.scanner().nextLine();

        agenda.imprimir();
        menuInicial();
    }

    public static Contato inserirContato() {
        System.out.println("Digite o nome do contato: ");
        String nome = IO.scanner().nextLine();

        Contato contato = new Contato(nome);

        System.out.println("Gostaria de adicionar dados a este contato? (s/n)");
        String adicionarDados = IO.scanner().nextLine();
        while(adicionarDados.equals("s")){
            contato.inserirDado(pegarDadosContato());

            System.out.print("...\nDado inserido com sucesso!");
            IO.scanner().nextLine();

            System.out.println("Gostaria de adicionar mais dados a este contato? (s/n)");
            adicionarDados = IO.scanner().nextLine();
        }

        return contato;
    }

    public static DadosContato pegarDadosContato() {
        TipoDado tipo = TipoDado.CELULAR;

        System.out.println("Qual dado gostaria de inserir: ");
        System.out.println(" (1) Celular ");
        System.out.println(" (2) Telefone ");
        System.out.println(" (3) Email ");
        System.out.println(" (4) Site ");

        Integer tipoSelecionado;

        do {
            tipoSelecionado = IO.scanner().nextInt();
        }
        while (tipoSelecionado < 1 || tipoSelecionado > 4);

        if(tipoSelecionado == 1)
            tipo = TipoDado.CELULAR;
        else if(tipoSelecionado == 2)
            tipo = TipoDado.TELEFONE;
        else if(tipoSelecionado == 3)
            tipo = TipoDado.EMAIL;
        else if(tipoSelecionado == 4)
            tipo = TipoDado.SITE;

        System.out.print("Digite dado a inserir: ");
        String valor = IO.scanner().nextLine();

        return new DadosContato(tipo, valor);
    }

    private static Scanner scanner(){
        return new Scanner(System.in);
    }
}
