package br.com.itau;

import java.util.ArrayList;

public class AgendaContatos implements Agenda {
    private String nome;
    private ArrayList<Contato> contatos;

    public AgendaContatos(String nome) {
        this.nome = nome;
        this.contatos = new ArrayList<Contato>();
    }

    @Override
    public void adicionar() {
        Contato contato = IO.inserirContato();
        this.contatos.add(contato);
    }

    @Override
    public void remover(Contato contato) {
        this.contatos.remove(contato);
    }

    @Override
    public void imprimir() {
        for (Contato contato : contatos)
            contato.imprimir();
    }

    public void exibirPorDado(TipoDado tipo, String valor) {

    }
}
