package br.com.itau;

import java.util.ArrayList;
import java.util.Map;

public class Contato {
    private String nome;
    private ArrayList<DadosContato> dados;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public ArrayList<DadosContato> getDados() {
        return dados;
    }

    public void setDados(ArrayList<DadosContato> dados) {
        this.dados = dados;
    }

    public Contato(String nome){
        this.nome = nome;
        this.dados = new ArrayList<DadosContato>();
    }

    public void inserirDado(DadosContato dadosContato) {
        this.dados.add(dadosContato);
    }

    public void imprimir(){
        System.out.println("-----------------------------------------");
        System.out.println("Nome: " + this.nome);

        for (DadosContato dado : dados) {
            System.out.println(dado.getTipo() + ": " + dado.getValor());
        }
    }
}
