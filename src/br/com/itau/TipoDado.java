package br.com.itau;

public enum TipoDado {
    CELULAR,
    TELEFONE,
    EMAIL,
    SITE
}
