package br.com.itau;

public interface Agenda {
    void adicionar();
    void remover(Contato contato);
    void imprimir();
}
